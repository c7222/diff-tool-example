#! /usr/bin/env bash

set -eu
cd "$(dirname "$0")" || exit

PATH=/opt/homebrew/bin:$PATH

REPO_ROOT=$(git rev-parse --show-toplevel)
PROJECT_PATH=$REPO_ROOT

function parse_args() {
  while [[ $# -gt 0 ]]; do
    key="$1"

    case $key in
    # Unknown option.
    *)
      # Save it in an array for later,
      POSITIONAL+=("$1")
      # Shift past argument.
      shift
      ;;
    esac
  done
  # Restore positional parameters.
  set -- "${POSITIONAL[@]}"
}


function install_tools() {
  if ! which pylint >/dev/null; then
    echo "              >>> pylint not installed, install pylint via homebrew ..."
    brew install pylint
  fi
  
  if ! which glab >/dev/null; then
    echo "              >>> glab not installed, install glab via homebrew ..."
    brew install glab
  fi
  
  glab auth login -h gitlab.com
}

parse_args "$@"

if [[ $1 =~ ^(install_tools|diff)$ ]]; then
  "$@"
else
  echo "Invalid subcommand $1" >&2
  echo "Usage:
    ./run.sh install_tools      : Install tooling, for example pytlint and glab, etc.
    ./run.sh diff               : Wrap css diff, for example ./run.sh master
  "
  exit 1
fi
