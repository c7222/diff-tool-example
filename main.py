# -*- coding: utf-8 -*-

import requests

API_KEY = '0701243d5e20f06dba0d47b8435e77c2'
CITY = 'Shanghai'  # Replace with the city you want to check the weather for
UNITS = 'imperial'  # Replace with 'metric' for Celsius

# Make a request to the OpenWeatherMap API
url = f'http://api.openweathermap.org/data/2.5/weather?q={CITY}&appid={API_KEY}&units={UNITS}'
response = requests.get(url)

# If the request was successful, print the weather information
if response.status_code == 200:
    weather_data = response.json()
    description = weather_data['weather'][0]['description']
    temperature = weather_data['main']['temp']
    print(f'The weather in {CITY} is {description} \
    with a temperature of {temperature:.1f} degrees {UNITS}.')
else:
    print(f'Error {response.status_code}: {response.reason}')
